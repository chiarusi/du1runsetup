# XML Parser for dm.file
# Author: T. Chiarusi
# Version: 1.0 
# Date: 08/06/2015

import math

from array import *
CrisMap = array('i')



# =========== Customization


# =========== Map Searching
# note it is important  the Map to be sorted from a the larger negative Voltage (-700 V) 
# to the smallest, because in the function  below we stop when the voltage difference 
# re-start growing...


def FindClosestADC(volt,CrisMap):
	MapLen = len(CrisMap)
#	print 'MapLen:\t', MapLen
	j=0
	previous_volt_difference=100000000
	current_volt_difference=0
	ok_adc =0
	ok_volt = -700
	while j<MapLen:
		CrisVolt = CrisMap[j]
		current_volt_difference=math.fabs(volt-CrisVolt)
		if(previous_volt_difference<current_volt_difference):
#			print 'test V:\t',volt,'\tMap:\t',CrisVolt,'\tpv_diff:\t',previous_volt_difference,'\tcr_diff:\t',current_volt_difference,'\tok_ADC:\t',ok_adc,'\tok_V:\t',ok_volt
			return ok_adc
		previous_volt_difference=current_volt_difference
		ok_adc =j
		ok_volt = CrisVolt
		j+=1



# ========== Arguments

import sys
  # variation of HV  (in Volt)
dm_file = sys.argv[1] # read the dm.file  as the first argument from the command line
print 'Processing ',dm_file

# ============ XML Parser

import xml.etree.ElementTree as ET


#==============




tree = ET.parse(dm_file)
root = tree.getroot()



j=0 # counter  for pmt_hvolt	

for CFG in root.findall('CurrentRunsetup/Setup/ConfigurationGroup'):

	Filter = CFG.find('Filter')		
	
	PBS = Filter.find('PBS').text
	Version = int(Filter.find('Version').text)
	SNo = int(Filter.find('SerialNumber').text)
	if PBS == "3.4.2.3" and  Version == 0 and  SNo == 0:
		for IDRP in CFG.findall('Optics/OperationalParameters/InputDiscreteRealParameter'):
#			print 'Found Global Definition'
			nome = IDRP.find('Name').text
			if (nome == "pmt_highvolt"):
				jj=0
				for AccVal in IDRP.findall('AcceptableValues/D2R'): 
					AccValADC = AccVal.find('I').text
					AccValVolt = AccVal.find('R').text
					CrisMap.insert(int(AccValADC),int(AccValVolt))
#					print jj     ,' ',AccValADC,' ',AccValVolt
					jj +=1

	elif PBS == "3.4.2.3" and  Version > 0 and  SNo > 0:
		for IDRP in CFG.findall('Optics/OperationalParameters/InputDiscreteRealParameter'):
			nome = IDRP.find('Name').text
	 		if (nome == "pmt_highvolt"):
	 			pmthv = IDRP.find('Value')
 				pmthv_adc= int(pmthv.text)	
#				if (pmthv_adc >= 0):
				print PBS, Version, SNo, pmthv_adc, CrisMap[pmthv_adc]
				j+=1

print 'Number of parsed PMT:\t', j,


