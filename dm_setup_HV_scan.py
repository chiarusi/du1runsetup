# XML Parser for dm.file
# Author: T. Chiarusi
# Version: 1.0 
# Date: 25/05/2015

import math



# =========== Customization

# range for scanning 
max_delta_HV = 150  # Volt
min_delta_HV = -50   # Volt
step = 25 # volt

# limits for PMT HV  in Volt
clow = -700.  # 0 ADC
chigh= -1500.  # 255 ADC
crange = chigh-clow

first_time =1

# =========== Conversion functions

def Digit2Volt(val):
	# convert from ADC digits in  Volt units 
	 x = (clow + val*(crange)/255.)
	 xabs = math.fabs(x)
	 sign = x/xabs
	 return int(sign*math.floor(xabs))

def Volt2Digit(val):
	# convert from Volt units in ADC digits 
	x = int(math.floor((val-clow)*255./crange))	
	if (x==0):
		print val
	xabs = math.fabs(x)
	sign = x/xabs
	return int(sign*math.floor(xabs))	


# ========== Arguments

import sys
  # variation of HV  (in Volt)
dm_file = sys.argv[1] # read the dm.file  as the first argument from the command line
print 'Processing ',dm_file

# ============ XML Parser

import xml.etree.ElementTree as ET
tree = ET.parse(dm_file)
root = tree.getroot()

#==============


def WriteFile(D_HV):
		
	j=0 # counter  for pmt_hvolt


	for IDRP in root.findall('CurrentRunsetup/Setup/ConfigurationGroup/Optics/OperationalParameters/InputDiscreteRealParameter'):
		nome = IDRP.find('Name').text
		if (nome == "pmt_highvolt"):
			pmthv = IDRP.find('Value')
			pmthv_adc= int(pmthv.text)
			if(pmthv_adc>50):
				pmthv_volt = Digit2Volt(pmthv_adc)
				pmthv2_volt = pmthv_volt+D_HV
				pmthv2_adc = Volt2Digit(pmthv2_volt)
				if(pmthv_adc>=0):  # if the outcome is good (i.e. !<0)
					if(first_time==1):
						print 'PMT_HV: ',pmthv_adc, pmthv_volt, pmthv2_volt, pmthv2_adc
					pmthv.text = str(pmthv2_adc)
#					pmthv.set('updated','yes')
					AccValADC = IDRP.find('AcceptableValues/D2R/I')
					AccValADC.text=str(pmthv2_adc)
					AccValVolt = IDRP.find('AcceptableValues/D2R/R')
					AccValVolt.text=str(pmthv2_volt)
					j+=1
	print 'Found ',j,' Input pmt_highvolt values'


	newfile = 'dm.detectorfile_scanHV_'+ str(D_HV) + '_nominalTHD'
	print 'Writing ', newfile
	with open(newfile,'w') as f:
		f.write("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n")
		tree.write(f)




#==============  MAIN LOOP


delta = max_delta_HV
while delta >= min_delta_HV:
	WriteFile(delta)
	first_time = 0
	print 'delta_HV: ',delta
	delta -= step