# Check conversion functions
# Author: T. Chiarusi
# Version: 1.0 
# Date: 02/06/2015

import math

from array import *
CrisMap = array('i')


# =========== Customization

# range for scanning 
max_HV = -700  # Volt
min_HV = -1500   # Volt
step = 3.14# volt

# limits for PMT HV  in Volt
clow = -700.  # 0 ADC
chigh= -1500.  # 255 ADC
crange = chigh-clow

first_time =1

# =========== Conversion functions

def Digit2Volt(val):
	# convert from ADC digits in  Volt units 
	 x = round(clow + val*(crange)/255.)
	 xabs = math.fabs(x)
	 sign = x/xabs
	 return int((sign*xabs))

def Volt2Digit(val):
	# convert from Volt units in ADC digits 
	x = round((val-clow)*255./crange)	
	if (x==0):
		print val
		sign =1 
		xabs =0
	else :
		xabs = math.fabs(x)
		sign = x/xabs
	return int((sign*xabs))
	

	
# ========== Arguments

import sys
  # variation of HV  (in Volt)
dm_file = sys.argv[1] # read the dm.file  as the first argument from the command line
print 'Processing ',dm_file

# ============ XML Parser

import xml.etree.ElementTree as ET
tree = ET.parse(dm_file)
root = tree.getroot()

#==============	

for IDRP in root.findall('CurrentRunsetup/Setup/ConfigurationGroup/Optics/OperationalParameters/InputDiscreteRealParameter'):
	nome = IDRP.find('Name').text
	if (nome == "pmt_highvolt"):
		pmthv = IDRP.find('Value')
		pmthv_adc= int(pmthv.text)
		if(pmthv_adc==0):
			print 'trovato'
			jj=0
			for AccVal in IDRP.findall('AcceptableValues/D2R'): 
				AccValADC = AccVal.find('I').text
				AccValVolt = AccVal.find('R').text
				CrisMap.insert(int(AccValADC),int(AccValVolt))
#				print jj     ,' ',AccValADC,' ',AccValVolt
				jj +=1

ic =0
HV = max_HV
c1 =0
c2 =0
while HV > min_HV-2:
	HV_adc = Volt2Digit(HV)
	HV_V   = Digit2Volt(HV_adc)
	MHV_V   = Digit2Volt(HV_adc)
#	print ic,'HV:\t',HV,'  \t2_ADC:\t',HV_adc,' \tADC_2_V:\t',HV_V,' \tmyD2R(',HV_adc,'):\t',MHV_V,' \tCris_V(',HV_adc,'):\t',CrisMap[HV_adc]
	print ic,'HV:\t',HV,'  \t2_ADC:\t',HV_adc,' \tADC_2_V:\t',HV_V,' \tCris_V(',HV_adc,'):\t',CrisMap[HV_adc]
#	diff = math.fabs(HV*1. - CrisMap[HV_adc]*1.)
	diff = math.fabs(HV_V*1. - HV*1.)
	
	if (diff >1):
		
		if (diff <2):
			c1 += 1
			print c1,' ',diff
			
		else:
			c2 +=1
			
	HV -= step
	ic +=1
	
print  '1<=x<2:',c1,' x>=2: ',c2	