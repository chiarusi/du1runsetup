# XML Parser for dm.file
# Author: T. Chiarusi
# Version: 1.0 
# Date: 04/06/2015

import math

from array import *
CrisMap = array('i')

# =========== Customization

# range for scanning 
max_delta_HV = 150  # Volt
min_delta_HV = -150   # Volt
step = 25 # volt

# limits for PMT HV  in Volt
clow = -700.  # 0 ADC
chigh= -1500.  # 255 ADC
crange = chigh-clow

first_time =1

# =========== Map Searching
# note it is important  the Map to be sorted from a the larger negative Voltage (-700 V) 
# to the smallest, because in the function  below we stop when the voltage difference 
# re-start growing...


def FindClosestADC(volt,CrisMap):
	MapLen = len(CrisMap)
#	print 'MapLen:\t', MapLen
	j=0
	previous_volt_difference=100000000
	current_volt_difference=0
	ok_adc =0
	ok_volt = -700
	while j<MapLen:
		CrisVolt = CrisMap[j]
		current_volt_difference=math.fabs(volt-CrisVolt)
		if(previous_volt_difference<current_volt_difference):
#			print 'test V:\t',volt,'\tMap:\t',CrisVolt,'\tpv_diff:\t',previous_volt_difference,'\tcr_diff:\t',current_volt_difference,'\tok_ADC:\t',ok_adc,'\tok_V:\t',ok_volt
			return ok_adc
		previous_volt_difference=current_volt_difference
		ok_adc =j
		ok_volt = CrisVolt
		j+=1



# ========== Arguments

import sys
  # variation of HV  (in Volt)
dm_file = sys.argv[1] # read the dm.file  as the first argument from the command line
print 'Processing ',dm_file

# ============ XML Parser

import xml.etree.ElementTree as ET


#==============


def WriteFile(D_HV):
	tree = ET.parse(dm_file)
	root = tree.getroot()

		
	j=0 # counter  for pmt_hvolt


	for IDRP in root.findall('CurrentRunsetup/Setup/ConfigurationGroup/Optics/OperationalParameters/InputDiscreteRealParameter'):
		nome = IDRP.find('Name').text
		if (nome == "pmt_highvolt"):
			pmthv = IDRP.find('Value')
			pmthv_adc= int(pmthv.text)
			if(pmthv_adc==0):
				print 'Found Global Definition'
				jj=0
				for AccVal in IDRP.findall('AcceptableValues/D2R'): 
					AccValADC = AccVal.find('I').text
					AccValVolt = AccVal.find('R').text
					CrisMap.insert(int(AccValADC),int(AccValVolt))
					print jj     ,' ',AccValADC,' ',AccValVolt
					jj +=1
			if(pmthv_adc>50):
				
				pmthv_volt = CrisMap[pmthv_adc]
				pmthv2_volt = pmthv_volt+D_HV
#				print 'Modified Voltage: ',pmthv2_volt,' V  from Original Voltage: ',pmthv_volt,' V'
				pmthv2_adc = FindClosestADC(pmthv2_volt,CrisMap)

#				pmthv_volt = Digit2Volt(pmthv_adc)
#				pmthv2_volt = pmthv_volt+D_HV
#				pmthv2_adc = Volt2Digit(pmthv2_volt)
				

				if(pmthv2_adc>=0):  # if the outcome is good (i.e. !<0)
#					if(first_time==1):
#						print 'PMT_HV: ',pmthv_adc, pmthv_volt, pmthv2_volt, pmthv2_adc,CrisMap[pmthv2_adc]
					pmthv.text = str(pmthv2_adc)
#					pmthv.set('updated','yes')
					AccValADC = IDRP.find('AcceptableValues/D2R/I')
					AccValADC.text=str(pmthv2_adc)
#					print 'DEBUG -----> written:\t',AccValADC.text,'\tcomputed:\t',pmthv2_adc,'\toriginal:\t',pmthv_adc,'\tDeltaHV:\t',D_HV
					print 'PLOT ',pmthv2_adc-pmthv_adc,' ',D_HV
					AccValVolt = IDRP.find('AcceptableValues/D2R/R')
					AccValVolt.text=str(CrisMap[pmthv2_adc])
					j+=1
				else:
					print 'WARNING: SOME PROBLEMS'
	print 'Found ',j,' Input pmt_highvolt values'


	newfile = 'dm.detectorfile_scanHV_'+ str(D_HV) + '_nominalTHD'
	print 'Writing ', newfile
	with open(newfile,'w') as f:
		f.write("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n")
		tree.write(f)




#==============  MAIN LOOP


delta = max_delta_HV
while delta >= min_delta_HV:
	WriteFile(delta)
	first_time = 0
	print 'delta_HV: ',delta
	delta -= step