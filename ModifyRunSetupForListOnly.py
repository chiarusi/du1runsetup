# ModifyRunSetupForListOnly
# Author: T. Chiarusi
# Version: 1.0 
# Date: 16/06/2015
#
#
#  This programs take a list of PMTs organized as
# 
#   DOMID  ChannelID   Version SerialNumber  HV_ADC  HV_Volt
#
#  and searches for the listed PMTs in the dm.detectorfile (May 2015 version).  
#  Once the listed PMT is found, the dm.detectorfile is modified according to the list info  

import math

from array import *
CrisMap = array('i')


#=========== CLASS PMT
class PMT(object):
	def __init__(self,DOM=None,Channel=None,Version=None,SNo=None,ADC=None,HV=None):
		self.DOM=DOM
		self.Channel=Channel 
		self.Version=Version
		self.SNo=SNo
		self.ADC = ADC
		self.HV = HV

PMTlist = []





# =========== Map Searching
# note it is important  the Map to be sorted from a the larger negative Voltage (-700 V) 
# to the smallest, because in the function  below we stop when the voltage difference 
# re-start growing...


def FindClosestADC(volt,CrisMap):
	MapLen = len(CrisMap)
#	print 'MapLen:\t', MapLen
	j=0
	previous_volt_difference=100000000
	current_volt_difference=0
	ok_adc =0
	ok_volt = -700
	while j<MapLen:
		CrisVolt = CrisMap[j]
		current_volt_difference=math.fabs(volt-CrisVolt)
		if(previous_volt_difference<current_volt_difference):
#			print 'test V:\t',volt,'\tMap:\t',CrisVolt,'\tpv_diff:\t',previous_volt_difference,'\tcr_diff:\t',current_volt_difference,'\tok_ADC:\t',ok_adc,'\tok_V:\t',ok_volt
			return ok_adc
		previous_volt_difference=current_volt_difference
		ok_adc =j
		ok_volt = CrisVolt
		j+=1






# ========== Arguments

import sys
  # variation of HV  (in Volt)
dm_file = sys.argv[1] # read the dm.file  as the first argument from the command line
l_file = sys.argv[2] # read the list file as second argument
newfile = sys.argv[3] # read the output file
print 'Processing ',dm_file,' according to ',l_file




#============== List File Parser

def MakePMTList():
	lf=open(l_file,'r')
	iPMT=0
	for line in lf:
		lins =line.split()
		PMTlist.append(PMT(lins[0],lins[1],lins[2],lins[3],lins[4],lins[5]))
#		print line.split()[1], line.split()[2]
#		print PMTlist[iPMT].Channel, PMTlist[iPMT].SNo
		iPMT +=1

	print 'Made PMT List with\t',iPMT,'\tPMTs'


# ============ XML Parser

import xml.etree.ElementTree as ET

#=============

def MakeConversionTable():

	tree = ET.parse(dm_file)
	root = tree.getroot()

	for CFG in root.findall('CurrentRunsetup/Setup/ConfigurationGroup'):

		Filter = CFG.find('Filter')		
	
		PBS = Filter.find('PBS').text
		Version = int(Filter.find('Version').text)
		SNo = int(Filter.find('SerialNumber').text)
		if PBS == "3.4.2.3" and  Version == 0 and  SNo == 0:
			for IDRP in CFG.findall('Optics/OperationalParameters/InputDiscreteRealParameter'):
#				print 'Found Global Definition'
				nome = IDRP.find('Name').text
				if (nome == "pmt_highvolt"):
					jj=0
					for AccVal in IDRP.findall('AcceptableValues/D2R'): 
						AccValADC = AccVal.find('I').text
						AccValVolt = AccVal.find('R').text
						CrisMap.insert(int(AccValADC),int(AccValVolt))
#						print jj     ,' ',AccValADC,' ',AccValVolt
						jj +=1
	print 'Made Conversion table'
# *****



def ScanPMT():

	tree = ET.parse(dm_file)
	root = tree.getroot()



	jMatchedPMT=0 # counter  for matched PMTs	
	jUnMatchedPMT=0 # counter for un-matched PMTs
	
	for CFG in root.findall('CurrentRunsetup/Setup/ConfigurationGroup'):

		Filter = CFG.find('Filter')		
	
		PBS = Filter.find('PBS').text
		Version = int(Filter.find('Version').text)
		SNo = int(Filter.find('SerialNumber').text)
	
		if PBS == "3.4.2.3" and  Version > 0 and  SNo > 0:
			PMTmatch = 0
			for pmt in PMTlist:
#				print pmt.Version, Version,' ',pmt.SNo,SNo
				if int(pmt.Version) == Version and int(pmt.SNo) == SNo: #matched
					jMatchedPMT+=1
					PMTmatch = 1
					#print pmt.Version, Version,' ',pmt.SNo,SNo
					for IDRP in CFG.findall('Optics/OperationalParameters/InputDiscreteRealParameter'):
						nome = IDRP.find('Name').text
	 					if (nome == "pmt_highvolt"):
	 						pmthv = IDRP.find('Value')
 							#pmthv_adc= int(pmthv.text)	
							#print PBS, Version, SNo, pmthv_adc, CrisMap[pmthv_adc],'\t==>\t',pmt.DOM,pmt.ADC,pmt.HV
							
							pmthv.text = pmt.ADC
							AccValADC = IDRP.find('AcceptableValues/D2R/I')
 							AccValADC.text=pmt.ADC
 							AccValVolt = IDRP.find('AcceptableValues/D2R/R')
 							AccValVolt.text=str(CrisMap[int(pmt.ADC)])
							#print pmthv.text,AccValADC.text,AccValVolt.text
							
			if PMTmatch == 0:   # no match after looping over the PMTlist
				jUnMatchedPMT +=1
			 	for IDRP in CFG.findall('Optics/OperationalParameters/InputDiscreteRealParameter'):
					nome = IDRP.find('Name').text
	 				if (nome == "pmt_highvolt"):
	 					pmthv = IDRP.find('Value')
	 					pmthv.text = "0"
						AccValADC = IDRP.find('AcceptableValues/D2R/I')
 						AccValADC.text="0"
 						AccValVolt = IDRP.find('AcceptableValues/D2R/R')
 						AccValVolt.text=str(CrisMap[0])
						#print pmthv.text,AccValADC.text,AccValVolt.text			
				
#			print '-------\n'
	print 'Number of matched PMT:\t', jMatchedPMT,'\t and un-matched PMTs:\t',jUnMatchedPMT

#PRINT FILES
	
	print 'Writing ', newfile
	with open(newfile,'w') as f:
		f.write("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n")
	 	tree.write(f)

#========= MAIN
MakePMTList()
MakeConversionTable()
ScanPMT()
